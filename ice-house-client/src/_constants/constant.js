const constants = {
    API_URL: 'https://localhost:44357/api',
    REQUIRE_INTERCEPTORS: {
        requestHandlerEnabled: true,
        responseHandlerEnabled: true
    }
};

export default constants;