const GetJsonFromLocalStorage = (key) => JSON.parse(localStorage.getItem(key));

const GetLoggedInUserToken = () => GetJsonFromLocalStorage('user').token;

export {
    GetJsonFromLocalStorage,
    GetLoggedInUserToken
}