import React from 'react'

const Dashboard = (props) => {
    return (
        <>
            <h1>Hi! I am Dashboard</h1>
            <button onClick={() => props.history.push('/test')}>Go to Test</button>
        </>
    )
}

export default Dashboard;