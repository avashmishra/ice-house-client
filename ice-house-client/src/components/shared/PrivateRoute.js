import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const PrivateRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={props => {
            const isAuthenticated = true;

            if (isAuthenticated) {
                return (
                    <>
                        <Component {...props} />
                    </>
                )
            }

            return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
        }} />
    )
}