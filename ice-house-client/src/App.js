import React from 'react';
import { Router, Route, Switch } from "react-router-dom";
import logo from './logo.svg';
import './App.css';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Login from './components/authenticate/Login';
import { createBrowserHistory } from "history";
import Dashboard from './components/dashboard/Dashboard';
import { PrivateRoute } from './components/shared/PrivateRoute';
import Test from './components/dashboard/Test';

const hist = createBrowserHistory();

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      textAlign: "center",
      flexGrow: 1,
    },
  }),
);

function App() {
  return (
    <>
    <Router history={hist}>
      <Switch>
        <Route path="/Login" component={Login} />
        <PrivateRoute exact path="/" component={Dashboard} />
        <PrivateRoute path="/test" component={Test} />
      </Switch>
    </Router>
    </>
  );
}

export default App;
