import { GetLoggedInUserToken } from '../../_helpers/helper';

const isEnabled = (config, property) => config.hasOwnProperty(property) && config[property];

//handles requests going from app to server
const requestHandler = async (request) => {
    if (isEnabled(request, 'requestHandlerEnabled')) {
        request.headers['Authorization'] = 'Bearer ' + GetLoggedInUserToken();
    }
    request.headers['Content-Type'] = 'application/json';
    request.headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS';
    return request;
}

//handles responses coming from server to app
const successHandler = (response) => {
    if (isEnabled(response.config, 'responseHandlerEnabled')) {
        return response.data;
    }

    return response;
}

const errorHandler = (response) => {
    if (isEnabled(response.config, 'responseHandlerEnabled')) {
        console.log("I am handling error responses");
    }

    Promise.resolve(response.response).then(res => {
        if (typeof (res) === "undefined") {
            Alert({ success: false, message: "Oops! Something went wrong on the server. Please try again" });
            return;
        }

        //if 401 redirect to login page
        if (res.status === 401) {
            localStorage.removeItem('user');
            window.location = window.origin + '/login';
        }
        //if not authorized redirect to dashboard
        if (res.status === 403) {
            window.location = window.origin + '/';
        }
    });

    throw Error("Error");
}

export {
    requestHandler,
    successHandler,
    errorHandler
}