import axios from 'axios';
import constants from '../_constants/constants';
import { requestHandler, successHandler, errorHandler } from './interceptor/axios.interceptor';

const axiosInstance = axios.create({
    baseURL: constants.API_URL
})

axiosInstance.interceptors.request.use(
    request => requestHandler(request)
)

axiosInstance.interceptors.response.use(
    response => successHandler(response),
    err => errorHandler(err)
)

export default axiosInstance;